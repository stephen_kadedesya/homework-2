#!/usr/bin/env python
import numpy as np

def main():
    print "Welcome to the AIMS module"

def std(num_list):                                        # define a function that calculatesstandard deviation for list of numbers
    try:           
        average = np.average(num_list)                    # computation of average of numbers in list
        s = 0                                             #Initialising the sum to 0
        for x in num_list:                                # checks if the number is in the list
            d = x - average                               # Returns how far the number is, from the average
            s = s + d**2             
        sd = np.sqrt(s/len(num_list))                     # returns the standard deviation
        sdd = round(sd,5)                                 # returns the standard deviation upto 5 decimal points       
        
    except ZeroDivisionError:                             # Raises error if empty list of numbers is entered.
        raise ZeroDivisionError("The List was Empty")

    except TypeError:
        raise TypeError("The List was not a list of numbers")

    return sdd

def avg_range(file_list):                                 # define afunction to calculate average range
    try:
        range_list = []                                   #Initialising with empty list of range values
        for file in file_list:                            # Checks every file in the file_list
            my_file = open(file)                          # Opens the file in the file_list
            for line in my_file:                          #Checks line by line in the file
                if line.startswith('Range'):              # Checks for line in the file which starts with word range
                    list_new = [item.strip() for item in line.split (':')]#Picks the range value
                    range_list.append(int(list_new[1]))   # Adds the range value to empty list
            my_file.close()    
        Avg = np.average(range_list)                      # computes the average value of range values in the list
    except IOError:                  
        raise IOError("No such file or directory")
    return Avg


if __name__ == "__main__":
    main()

