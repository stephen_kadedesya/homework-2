from nose.tools import assert_equal

import aims
# Unit test of Standard deviation function
def test_ints():                             # Testing integer values
    num_list = [1, 2, 3, 4]
    obs = aims.std(num_list)
    exp = 1.11803
    assert_equal(obs, exp)


def test_negative():                         # Testing negative integer values

    num_list = [-1,-2,-3,-4]
    obs = aims.std(num_list)
    exp = 1.11803
    assert_equal(obs, exp)

def test_floatsandints():                    # Testing a combination og integrs and floats

    num_list = [1,2.5,3,4.5]
    obs = aims.std(num_list)
    exp = 1.25000
    assert_equal(obs, exp)

def test_neg_postive():                      #Testing both negative and postive integers
    num_list = [-1, 2, -3]
    obs = aims.std(num_list)
    exp = 2.05480
    assert_equal(obs, exp)

# Unit test of average Range function.
def test_avg1():
    files=['data/bert/audioresult-00215']    # Testing one list
    obs=aims.avg_range(files)
    exp= 5.0
    assert_equal(obs, exp)
    
def test_avg2():                             # Testing two files
    files=['data/bert/audioresult-00215', 'data/bert/audioresult-00384']
    obs=aims.avg_range(files)
    exp= 7.5
    assert_equal(obs, exp)
def test_avg3():                             # Testing three files
    files=['data/bert/audioresult-00215', 'data/bert/audioresult-00384', 'data/bert/audioresult-00534']
    obs=aims.avg_range(files)
    exp= 7.0
    assert_equal(obs, exp)
   
